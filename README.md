# REDE EMPRESARIAL

A equipe se propôs a desenvolver uma ferramenta que possa possibilitar auxiliar diversos tipos de clientes na escolha e implementação de suas redes. Tomamos como base ajudar comerciantes e pequenos a darem o ponta pé inicial na escolha da sua internet e conectividade.

Além de tornar viável e bem intuitiva, nossa ferramenta servirá como suporte, analisando e avaliando a internet do cliente. Responsável também por medir a velocidade, conectividade e segurança dos usuários. 


MENTORIA: 

---> FÁBIO ANTÔNIO RIBEIRO DA SILVA SIQUEIRA - LÍDER (https://gitlab.com//fabio.siqueira)
---> ALEXANDRE ALVES DA SILVA FILHO (https://gitlab.com/alexandrefilho)
